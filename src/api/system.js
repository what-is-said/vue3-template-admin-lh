import request from '@/utils/request'

// 登录
export const login = (data) => {
	data.requestSource = 'WEBPAGE'
	return request({
		url: '/api/passport/by-password',
		method: 'post',
		data,
	})
}

// 退出登录
export const logout = () => {
	return request({
		url: '/logout',
		method: 'post',
	})
}

// 获取用户所拥有的点击权限
export const userPermission = (data) => {
	return request({
		url: '/admin/role-info/my-list',
		method: 'post',
		data,
	})
}

// 获取路由表数据
export const userRouterList = () => {
	return request({
		url: '/admin/menu/list',
		method: 'post',
		data: { type: 1 },
	})
}
