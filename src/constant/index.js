export const TOKEN = 'token'
export const USERINFO = 'user_info'

export const TIME_STAMP = 'time_stamp'
export const TIME_STAMP_VALUE = 2 * 3600 * 1000 // 设置失效时长

export const LANG = 'langLocale' // 语言包

export const MAIN_COLOR = 'main_color' // 主题色
export const DEFAULT_COLOR = '#409eff' // 默认主题色
export const ELE_PLUS_STYLE = 'ele_plus_style'
export const FORMULA = {
	'shade-1': 'color(primary shade(10%))',
	'light-1': 'color(primary tint(10%))',
	'light-2': 'color(primary tint(20%))',
	'light-3': 'color(primary tint(30%))',
	'light-4': 'color(primary tint(40%))',
	'light-5': 'color(primary tint(50%))',
	'light-6': 'color(primary tint(60%))',
	'light-7': 'color(primary tint(70%))',
	'light-8': 'color(primary tint(80%))',
	'light-9': 'color(primary tint(90%))',
	subMenuHover: 'color(primary tint(70%))',
	subMenuBg: 'color(primary tint(80%))',
	menuHover: 'color(primary tint(90%))',
	menuBg: 'color(primary)',
} // 预定义色值表

export const TAGS_VIEW = 'tagsView' // 标签

export const ACTIVE_ROUTER = 'activeRouter' // 激活路由

// 权限数组
export const roleArr = {
	// 角色管理
	ROLEMANAGE: {
		add: 'ROLE_MANAGER_ADD',
		delete: 'ROLE_MANAGER_DETELE',
		edit: 'ROLE_MANAGER_EDIT',
		check: 'ROLE_MANAGER_VIEW_LIST',
	},
}
