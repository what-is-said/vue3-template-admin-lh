// 导入所有svg图片文件
// const svgRequire = import.meta.globEager('/src/icons/svg/*.svg')
import iconSvg from '@/components/svgIcon/index.vue' // 导入全部icon-svg组件

export default (app) => {
	app.component('svg-icon', iconSvg)
}

// export default (app) => {
// 	Object.keys(svgRequire).forEach((svgIcon) => {
// 		const component = svgRequire[svgIcon]?.default
// 		// 挂载全局控件
// 		console.log(component)
// 		app.component('svg-icon', component)
// 	})
// }
