import variables from '@/styles/variables.module.scss'
import {
	LANG,
	MAIN_COLOR,
	DEFAULT_COLOR,
	ELE_PLUS_STYLE,
	ACTIVE_ROUTER,
	TAGS_VIEW,
} from '@/constant'
import { setStorage, getStorage } from '@/utils/storage'
import { generateColors } from '@/utils/theme'
import { htmlTitle } from '@/utils/tags'
import router from '@/router/index'

// state状态
const state = () => ({
	menuList: [],
	menuVar: variables,
	menuCollapse: false, // 菜单折叠 true折叠 false不折叠
	language: getStorage(LANG) || 'zh', // 默认中文
	mainColor: getStorage(MAIN_COLOR) || DEFAULT_COLOR, // 主题色
	elePlusStyle: getStorage(ELE_PLUS_STYLE) || '',
	activeRouter: getStorage(ACTIVE_ROUTER) || '', // 当前的激活路由
	tagsList: getStorage(TAGS_VIEW) || [], // 保存的tags列表
})

// 计算
const getters = {
	// 菜单样式
	menuStyle(state) {
		return {
			...state.menuVar,
			...generateColors(state.mainColor),
		}
	},
	// 折叠状态
	collapseStatus(state) {
		return state.menuCollapse
	},
	// 国际化语言包
	languagePackage(state) {
		return state.language
	},
	// 主题色
	themeColor(state) {
		return state.mainColor
	},
	// 样式表
	cacheEleStyle(state) {
		return state.elePlusStyle
	},
	// tags列表
	tagsViewList(state) {
		return state.tagsList
	},
	// 当前激活路由
	currentActiveRouter(state) {
		return state.activeRouter
	},
}

// 同步
const mutations = {
	menuCollapseStatus(state) {
		state.menuCollapse = !state.menuCollapse
	},
	// 设置国际化
	setLanguage(state, lang) {
		state.language = lang
		setStorage(LANG, lang)
	},
	// 设置主题色
	setThemeColor(state, color) {
		state.mainColor = color
		state.menuVar.menuBg = color // 修改主题色
		setStorage(MAIN_COLOR, color)
	},
	// 保存获取的样式表
	setStyleFromInter(state, styleData) {
		state.elePlusStyle = styleData
		setStorage(ELE_PLUS_STYLE, styleData)
	},
	// 当前激活路由
	setActiveRouter(state, router) {
		state.activeRouter = router
		setStorage(ACTIVE_ROUTER, router)
	},
	// 新增tag标签
	addTagsViewList(state, tags) {
		const len = state.tagsList.length
		const flag = state.tagsList.every((item) => item.path !== tags.path)
		if (len === 0 || flag) {
			state.tagsList.push(tags)
			setStorage(TAGS_VIEW, state.tagsList)
		}
	},
	// 替换tag数组
	replaceTagsViewList(state, tags) {
		state.tagsList = tags
		setStorage(TAGS_VIEW, state.tagsList)
	},
	// 修改指定tag的title
	editTagsView(state, { index, tag }) {
		state.tagsList[index] = tag
		setStorage(TAGS_VIEW, state.tagsList)
	},
	// 根据路由匹配对应tag
	routeByTagsView(state, { route, i18n, pageName }) {
		const { fullPath, meta, name, params, path, query } = route
		const title = i18n.t(`msg.route.${pageName}`)
		meta.title = pageName
		const tag = {
			fullPath,
			meta,
			name,
			params,
			query,
			path,
			title,
		}
		htmlTitle(title)
		const findIndex = state.tagsList.findIndex(
			(item) => item.name === route.name
		)
		this.commit('app/editTagsView', { index: findIndex, tag })
	},
	// 移除指定tag
	removeTagsView(state, { type, clickRouter, flag = true }) {
		// flag 移除指定标签后是否跳转 默认跳转
		let newTagsList = null
		const currentIndex = state.tagsList.findIndex(
			(tag) => tag.name === clickRouter.name
		)
		if (type === 'index') {
			// 移除指定tag
			newTagsList = state.tagsList.filter((tag, i) => i !== currentIndex)
			if (clickRouter.name === state.activeRouter.name) {
				// 移除的是激活标签
				if (flag) {
					let nextRouter = null
					if (currentIndex - 1 >= 0) {
						nextRouter = newTagsList[currentIndex - 1]
					} else if (currentIndex + 1 <= newTagsList.length) {
						nextRouter = newTagsList[currentIndex]
					} else {
						nextRouter = { path: '/home', query: {} }
					}
					router.push({
						path: nextRouter.path,
						query: nextRouter.query ? { ...nextRouter.query } : {},
					})
				}
			}
		} else if (type === 'right') {
			// 移除右侧tag
			newTagsList = state.tagsList.filter((tag, i) => i <= currentIndex)
		} else if (type === 'other') {
			// 移除除自身以外的其他tag
			newTagsList = state.tagsList.filter((tag, i) => i === currentIndex)
		}
		state.tagsList = newTagsList
		setStorage(TAGS_VIEW, state.tagsList)
	},
}

// 异步
const actions = {}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations,
}
