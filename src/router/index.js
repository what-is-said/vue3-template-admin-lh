import * as vueRouter from 'vue-router'
const { createRouter, createWebHashHistory, createWebHistory } = vueRouter
const history =
	process.env.NODE_ENV === 'production'
		? createWebHistory()
		: createWebHashHistory()

import systemRoutes from './modules/system' // 静态路由
import privateRoutes from './modules/private' // 动态路由

export const routes = [...systemRoutes]

const router = createRouter({
	history,
	routes,
})

export default router
