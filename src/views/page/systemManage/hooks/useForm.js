import { nextTick, ref } from 'vue'

import { permissionList, addRole, updateRole } from '../api/role'
import { TipElMessage } from '@/utils/utils'
import { useRoute, useRouter } from 'vue-router'
import store from '@/store/index'
import i18n from '@/i18n/index'
import { deleteTagsView } from '@/utils/tags'

// 表单hooks
const useForm = () => {
	const router = useRouter() // 路由
	const route = useRoute()
	const { id, permissionCode, roleName } = JSON.parse(route.query.roleItem)
	const treeRef = ref(null)
	const currentActiveRouter = store.getters['app/currentActiveRouter']

	// 校验规则
	const rules = ref({
		roleName: [
			{
				required: true,
				message: '请输入角色名称',
				trigger: 'blur',
			},
		],
		permissionCode: [
			{
				type: 'array',
				required: true,
				validator: permissionValidator,
				trigger: 'blur',
			},
		],
	})
	// 表单变量
	const formData = ref({
		id: '', // 更新数据的id
		roleName: '', // 角色名
		permissionCode: [], // 权限数组
	})
	// 权限列表
	const powerList = ref([
		{
			label: '全部',
			operateCode: 'all',
			children: [],
		},
	])
	// 子树选择项
	const defaultProps = ref({
		children: 'children',
		label: 'label',
	})
	// 供主文件使用函数
	const callback = () => {
		getPermission()
			.then((res) => {
				powerList.value[0].children = res
				if (id) {
					// 编辑
					// 修改tags标签以及网页标题
					store.commit('app/routeByTagsView', {
						route,
						i18n: i18n.global,
						pageName: 'editRole',
					})
					formData.value.roleName = roleName
					formData.value.id = id
					let allParentId = []
					res.forEach((item) => {
						allParentId.push(item.parentId)
					})
					const checkedKeysArr = formatterChildNodeCode(res).filter(
						(item) => permissionCode.includes(item)
					)
					nextTick(() => {
						treeRef.value.setCheckedKeys(checkedKeysArr)
					})
				}
			})
			.finally(() => {})
	}
	// 权限列表当前点击项
	const currentCheck = (checkedItem, checkedNodes) => {
		formData.value.permissionCode = currentItem(checkedItem, checkedNodes)
	}

	// 提交成功
	const submitResolve = () => {
		submitSuccess(formData.value, router)
		deleteTagsView('index', currentActiveRouter, false)
	}
	// 提交失败
	const submitReject = () => {}

	// 取消表单
	const cancelResolve = () => {
		goRolePage(router)
		deleteTagsView('index', currentActiveRouter, false)
	}
	return {
		rules,
		treeRef,
		powerList,
		formData,
		defaultProps,
		callback,
		currentCheck,
		submitResolve,
		submitReject,
		cancelResolve,
	}
}

// 获取当前点击项数据
const currentItem = (checkedItem, checkedNodes) => {
	const { checkedKeys, halfCheckedKeys } = checkedNodes
	return [...halfCheckedKeys, ...checkedKeys].filter((i) => i !== 'all')
}

// 提交成功
const submitSuccess = (data, router) => {
	const { id, ...params } = data
	if (id) {
		// 编辑
		updateRole(data).then(() => {
			TipElMessage('编辑成功')
			goRolePage(router)
		})
	} else {
		addRole(params).then(() => {
			TipElMessage('新增成功')
			goRolePage(router)
		})
	}
}

// 获取权限列表
const getPermission = () => {
	return permissionList()
		.then((res) => {
			if (Object.is(res.status, 'OK')) {
				const { data } = res
				if (data.length > 0) {
					return Promise.resolve(formatterPermission(data))
				}
			}
		})
		.catch(() => Promise.resolve([]))
}

// 跳转到角色管理
const goRolePage = (router) => {
	router.push({ name: 'roleManage' })
}

// 迭代权限数据
const formatterPermission = (data) => {
	return data.map((item) => {
		item.label = item.permissionName
		if (item.child && item.child.length > 0) {
			item.children = formatterPermission(item.child)
		}
		return item
	})
}

// 迭代获取叶子节点
const formatterChildNodeCode = (data) => {
	let arr = []
	data.forEach((item) => {
		if (!item.child) {
			//  叶子节点
			arr.push(item.operateCode)
		} else {
			arr = [...arr, ...formatterChildNodeCode(item.child)]
		}
	})
	return arr
}

// 自定义权限列表验证函数
const permissionValidator = (rule, value, callback) => {
	if (value.length === 0) {
		callback(new Error('请至少选择一个权限'))
	} else {
		callback()
	}
}

export default useForm
