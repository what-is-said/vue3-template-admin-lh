import i18n from '@/i18n'

export const usePassWord = (rule, value, callback) => {
	if (value === '') {
		callback(new Error(i18n.global.t('msg.login.passwordPlaceholder')))
	} else if (value.length < 6) {
		callback(new Error(i18n.global.t('msg.login.passwordRule')))
	} else {
		callback()
	}
}
