// 全局变量配置

export default {
	// 基础路径
	baseurl: 'http://bs-test.api.tuopukeji.cn/',
	// 标题
	title: 'vue3-template-admin-lh',
	// 免登陆白名单
	whiteList: ['/login', '/404', '/401'],
}
