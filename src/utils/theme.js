import color from 'css-color-function'
import rgbHex from 'rgb-hex'
import { FORMULA } from '@/constant'
import axios from 'axios'

/**
 * 写入新样式到 style
 * @param {*} elNewStyle  element-plus 的新样式
 */
export const writeNewStyle = (elNewStyle) => {
	const headStyle = Array.from(document.head.children)
	let themeStyle = headStyle.find((i) => i.className)
	const style = document.createElement('style')
	style.className = 'theme'
	style.setAttribute('type', 'text/css')
	style.innerText = elNewStyle
	if (themeStyle) {
		document.head.insertBefore(style, themeStyle)
		document.head.removeChild(themeStyle)
	} else {
		//  创建一个style标签并插入到head中
		document.head.appendChild(style)
	}
}

/**
 * 根据主色值，生成最新的样式表
 * @param {*} themeColor
 * @returns
 */
export const generateNewStyle = async (themeColor, store) => {
	const colors = generateColors(themeColor)
	let ele_plus_css = await getElePlusCss(store)
	// 把生成的样式表用来替换原样式
	Object.keys(colors).forEach((key) => {
		ele_plus_css = ele_plus_css.replace(
			new RegExp('(:|\\s+)' + key, 'g'),
			'$1' + colors[key]
		)
	})
	return ele_plus_css
}

/**
 * 根据主色生成色值表
 * @param {*} primary 主色
 */
export const generateColors = (primary) => {
	if (!primary) return
	const colors = { primary }
	Object.keys(FORMULA).forEach((key) => {
		const value = FORMULA[key].replace(/primary/g, primary)
		colors[key] = `#${rgbHex(color.convert(value))}`
	})
	return colors
}

/**
 * 获取默认样式表
 */
const getElePlusCss = async (store) => {
	let eleStyle = ''
	eleStyle = store.getters['app/cacheEleStyle']
	if (eleStyle) return getStyleTemplate(eleStyle)
	const packageObj = import.meta.globEager('../../package.json')
	let ele_plus_version = ''
	Object.keys(packageObj).forEach((key) => {
		ele_plus_version = packageObj[key].default.dependencies['element-plus']
	})
	const requireUrl = `https://unpkg.com/element-plus@${ele_plus_version}/dist/index.css`
	const { data } = await axios(requireUrl)
	// 数据组装
	eleStyle = getStyleTemplate(data)
	store.commit('app/setStyleFromInter', eleStyle)
	return eleStyle
}

/**
 * 组装样式表
 * @param {*} data
 */
const getStyleTemplate = (data) => {
	// 要替换的默认色值
	const colorMap = {
		'#3a8ee6': 'shade-1',
		'#409eff': 'primary',
		'#53a8ff': 'light-1',
		'#66b1ff': 'light-2',
		'#79bbff': 'light-3',
		'#8cc5ff': 'light-4',
		'#a0cfff': 'light-5',
		'#b3d8ff': 'light-6',
		'#c6e2ff': 'light-7',
		'#d9ecff': 'light-8',
		'#ecf5ff': 'light-9',
	}
	// 根据默认色值标记需要替换的色值
	Object.keys(colorMap).forEach((color) => {
		data = data.replace(new RegExp(color, 'ig'), colorMap[color])
	})
	return data
}
