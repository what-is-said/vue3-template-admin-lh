import settings from '@/config/settings'
import store from '@/store/index'

/**
 * 是否需要缓存该tags标签，返回true不需要缓存
 * @param {*} path 路径
 * @returns
 */
export const isCacheTags = (path) => settings.whiteList.includes(path)

/**
 * 为每个路由设置一个唯一的title，并国际化
 * @param {*} route
 * @param {*} i18n
 */
export const getTitle = (route, i18n = '') => {
	let titleText = ''
	const {
		meta,
		meta: { isInternational, title },
	} = route
	if ((meta && !isInternational) || !meta || !i18n) {
		// 无 meta i18n 或 isInternational为false 取路径最后一个
		const titleArr = route.path.split('/')
		titleText = titleArr[titleArr.length - 1]
	} else {
		titleText = i18n.t(`msg.route.${title}`)
	}
	htmlTitle(titleText)
	return titleText
}

/**
 * 为网页标签设置唯一title
 * @param {*} name 路由名
 */
export const htmlTitle = (name) => {
	document.title = `${name}-${settings.title}`
}

/**
 * 删除指定tags
 * @param {*} type 类型
 * @param {*} clickRouter 路由
 */
export const deleteTagsView = (type, clickRouter, flag) => {
	store.commit('app/removeTagsView', {
		type,
		clickRouter,
		flag,
	})
}
