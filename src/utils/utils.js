import { ElMessage } from 'element-plus'

/**
 * 判断是否为外部资源
 * @param {*} path
 * @returns
 */
export const externalCheck = (path) => {
	return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * 消息提示框
 * @param {*} message 消息提示
 * @param {*} type 状态
 * @param {*} showClose 是否显示关闭按钮
 */
export const TipElMessage = (
	message = '操作成功',
	type = 'success',
	showClose = true
) => {
	ElMessage({
		showClose,
		message,
		type,
	})
}
