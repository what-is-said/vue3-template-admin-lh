import store from '@/store/index'
import { roleArr } from '@/constant/index'

const checkPermission = (el, binding) => {
	const { rolePage, roleName } = binding.value
	const roleList = store.getters['user/permissions']
	const flag = roleList.some((roleItem) =>
		roleItem.permissionCode.includes(roleArr[rolePage][roleName])
	)
	// true有权限 false无权限 则删除该节点
	if (!flag) el.parentNode && el.parentNode.removeChild(el)
}
// 权限指令
const roleDirective = (app) => {
	app.directive('role', {
		// 在绑定元素的父组件被挂载后调用
		mounted(el, binding) {
			checkPermission(el, binding)
		},
		// 在包含组件的 VNode 及其子组件的 VNode 更新后调用
		update(el, binding) {
			checkPermission(el, binding)
		},
	})
}

export default roleDirective
